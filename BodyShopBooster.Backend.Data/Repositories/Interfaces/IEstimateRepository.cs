﻿using BodyShopBooster.Backend.Data.Dtos;
using System.Threading.Tasks;

namespace BodyShopBooster.Backend.Data.Interfaces
{
    public interface IEstimateRepository
    {
        Task AddEstimate(EstimateRequestDto estimateRequest);
        Task<EstimateResponseDto> GetEstimateById(int estimateId);
        Task UpdateEstimate(int estimateId, EstimateRequestDto estimateRequest);
    }
}
