﻿using AutoMapper;
using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Data.Entities;
using BodyShopBooster.Backend.Data.Interfaces;
using BodyShopBooster.Backend.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace BodyShopBooster.Backend.Data.Implementations
{
    public class EstimateRepository : BaseRepository, IEstimateRepository
    {
        public EstimateRepository(EstimateDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public async Task AddEstimate(EstimateRequestDto estimateRequest)
        {
            var estimate = _mapper.Map<Estimate>(estimateRequest);
            _dbContext.Estimates.Add(estimate);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<EstimateResponseDto> GetEstimateById(int estimateId)
        {
            var estimate = await _dbContext.Estimates.AsNoTracking().FirstOrDefaultAsync(est => est.Id == estimateId);
            return _mapper.Map<EstimateResponseDto>(estimate);
        }

        public async Task UpdateEstimate(int estimateId, EstimateRequestDto estimateRequest)
        {
            var existingEstimate = await GetEstimateById(estimateId);
            if (existingEstimate is null)
            {
                throw new InvalidOperationException("Estimate does not exist");
            }

            var estimate = _mapper.Map<Estimate>(estimateRequest);
            estimate.Id = estimateId;
            _dbContext.Update(estimate);
            await _dbContext.SaveChangesAsync();
        }
    }
}
