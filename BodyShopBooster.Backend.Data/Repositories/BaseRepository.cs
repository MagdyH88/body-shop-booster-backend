﻿using AutoMapper;

namespace BodyShopBooster.Backend.Data.Repositories
{
    public class BaseRepository
    {
        protected readonly IMapper _mapper;
        protected readonly EstimateDbContext _dbContext;

        public BaseRepository(EstimateDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
    }
}
