﻿using BodyShopBooster.Backend.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace BodyShopBooster.Backend.Data
{
    public class EstimateDbContext : DbContext
    {
        public EstimateDbContext(DbContextOptions<EstimateDbContext> options) : base(options)
        {
        }

        public virtual DbSet<Estimate> Estimates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
