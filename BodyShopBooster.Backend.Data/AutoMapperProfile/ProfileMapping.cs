﻿using AutoMapper;
using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Data.Entities;

namespace BodyShopBooster.Backend.Services.Mapper
{
    public class ProfileMapping: Profile
    {
        public ProfileMapping()
        {
            CreateMap<EstimateRequestDto, Estimate>();
            CreateMap<Estimate, EstimateResponseDto>();
        }       
    }
}
