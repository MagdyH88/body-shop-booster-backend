﻿using System.ComponentModel.DataAnnotations;

namespace BodyShopBooster.Backend.Data.Entities
{
    public class Estimate
    {
        public int Id { get; set; }
		[StringLength(30)]
		public string FirstName { get; set; }
		[StringLength(30)]
		public string LastName { get; set; }
		[StringLength(15)]
		public string CarType { get; set; }
		[StringLength(4)]
		public string Year { get; set; }
		[StringLength(15)]
		public string Model { get; set; }
		[StringLength(15)]
		public string LicensePlate { get; set; }
		[StringLength(15)]
		public string Status { get; set; }
	}
}
