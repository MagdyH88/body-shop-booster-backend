﻿namespace BodyShopBooster.Backend.Data.Dtos
{
    public class EstimateResponseDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CarType { get; set; }
        public string Year { get; set; }
        public string Model { get; set; }
        public string LicensePlate { get; set; }
        public string Status { get; set; }
    }
}
