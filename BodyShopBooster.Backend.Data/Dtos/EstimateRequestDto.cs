﻿namespace BodyShopBooster.Backend.Data.Dtos
{
    public class EstimateRequestDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CarType { get; set; }
        public string Year { get; set; }
        public string Model { get; set; }
        public string LicensePlate { get; set; }
        public string Status { get; set; }
    }
}
