﻿using AutoFixture;
using BodyShopBooster.Backend.Api.Controllers;
using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Services.Interfaces;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace BodyShopBooster.Backend.Api.Tests
{
    public class EstimateControllerTests
    {
        private readonly Mock<IEstimateService> _estimateServiceMock;
        private readonly EstimateController _estimateController;
        private readonly Mock<ILogger<EstimateController>> _logger;
        private readonly Fixture _fixture;

        public EstimateControllerTests()
        {
            _estimateServiceMock = new Mock<IEstimateService>();
            _logger = new Mock<ILogger<EstimateController>>();
            _fixture = new Fixture();

            _estimateController = new EstimateController(_estimateServiceMock.Object, _logger.Object);
        }

        [Fact(DisplayName = "PostEstimate should return success result")]
        public async Task PostEstimate_ShouldReturnSuccessResult()
        {
            //Arange
            var estimateRequest = _fixture.Create<EstimateRequestDto>();
            _estimateServiceMock.Setup(_ => _.CreateEstimate(estimateRequest));

            //Act
            var result = await _estimateController.PostEstimate(estimateRequest) as StatusCodeResult;

            //Assert
            result.StatusCode.Should().Be(201);
        }

        [Fact(DisplayName = "GetEstimateById should return success result")]
        public async Task GetEstimateById_ShouldReturnSuccessResult()
        {
            //Arange
            var estimateResponse = new EstimateResponseDto();
            _estimateServiceMock.Setup(_ => _.GetEstimateById(It.IsAny<int>())).ReturnsAsync(estimateResponse);

            //Act
            var result = await _estimateController.GetEstimateById(It.IsAny<int>()) as OkObjectResult;

            //Assert
            result.StatusCode.Should().Be(200);
            result.Value.Should().BeEquivalentTo(estimateResponse);
        }

        [Fact(DisplayName = "PutEstimate should return success result")]
        public async Task PutEstimate_ShouldReturnSuccessResult()
        {
            //Arange
            var estimateRequest = _fixture.Create<EstimateRequestDto>();
            _estimateServiceMock.Setup(_ => _.UpdateEstimate(It.IsAny<int>(), estimateRequest));

            //Act
            var result = await _estimateController.PutEstimate(It.IsAny<int>(), estimateRequest) as NoContentResult;

            //Assert
            result.StatusCode.Should().Be(204);
        }
    }
}
