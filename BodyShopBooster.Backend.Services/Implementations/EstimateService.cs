﻿using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Data.Interfaces;
using BodyShopBooster.Backend.Services.Interfaces;
using System.Threading.Tasks;

namespace BodyShopBooster.Backend.Services.Implementations
{
    public class EstimateService : IEstimateService
    {
        private readonly IEstimateRepository _estimateRepository;

        public EstimateService(IEstimateRepository estimateRepository)
        {
            _estimateRepository = estimateRepository;
        }

        public async Task CreateEstimate(EstimateRequestDto estimateRequest)
        {
            await _estimateRepository.AddEstimate(estimateRequest);
        }

        public async Task<EstimateResponseDto> GetEstimateById(int estimateId)
        {
            return await _estimateRepository.GetEstimateById(estimateId);
        }

        public async Task UpdateEstimate(int estimateId, EstimateRequestDto estimateRequest)
        {
            await _estimateRepository.UpdateEstimate(estimateId, estimateRequest);
        }
    }
}
