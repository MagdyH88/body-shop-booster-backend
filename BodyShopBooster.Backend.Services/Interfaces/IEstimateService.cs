﻿using BodyShopBooster.Backend.Data.Dtos;
using System.Threading.Tasks;

namespace BodyShopBooster.Backend.Services.Interfaces
{
    public interface IEstimateService
    {
        Task CreateEstimate(EstimateRequestDto estimateRequest);
        Task<EstimateResponseDto> GetEstimateById(int estimateId);
        Task UpdateEstimate(int estimateId, EstimateRequestDto estimateRequest);
    }
}
