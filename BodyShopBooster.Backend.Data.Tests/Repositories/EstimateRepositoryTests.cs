﻿using AutoFixture;
using AutoMapper;
using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Data.Entities;
using BodyShopBooster.Backend.Data.Implementations;
using BodyShopBooster.Backend.Data.Interfaces;
using BodyShopBooster.Backend.Services.Mapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BodyShopBooster.Backend.Data.Tests.Repositories
{
    public class EstimateRepositoryTests
    {
        private readonly Mock<EstimateDbContext> _mockContext;
        private readonly IEstimateRepository _estimateRepository;
        private readonly Fixture _fixture;

        public EstimateRepositoryTests()
        {
            _mockContext = new Mock<EstimateDbContext>(new object[] { new DbContextOptions<EstimateDbContext>() });
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProfileMapping());
            });
            _estimateRepository = new EstimateRepository(_mockContext.Object, config.CreateMapper());
            _fixture = new Fixture();
        }

        [Fact(DisplayName = "AddEstimate should add new estimate successfully")]
        public async Task AddEstimate_ShouldAddNewEstimateSuccessfully()
        {
            //Arrange:
            var estimateId = _fixture.Create<int>();
            var estimateRequest = _fixture.Create<EstimateRequestDto>();

            var estimate = new Estimate()
            {
                Id = estimateId,
                FirstName = _fixture.Create<string>()
            };

            var estimates = new List<Estimate>
            {
                estimate
            }.AsQueryable().BuildMockDbSet();

            _mockContext.Setup(_ => _.Set<Estimate>()).Returns(estimates.Object);
            _mockContext.Setup(_ => _.Estimates).Returns(estimates.Object);

            //Act:
            Func<Task> result = () => _estimateRepository.AddEstimate(estimateRequest);

            //Assert:
            await result.Should().NotThrowAsync();
        }
    }
}
