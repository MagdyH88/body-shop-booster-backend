﻿using AutoFixture;
using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Data.Interfaces;
using BodyShopBooster.Backend.Services.Implementations;
using BodyShopBooster.Backend.Services.Interfaces;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace BodyShopBooster.Backend.Services.Tests
{
    public class EstimateServiceTests
    {
        private readonly IEstimateService _estimateServiceMock;
        private readonly Mock<IEstimateRepository> _estimateRepositoryMock;
        private readonly Fixture _fixture;

        public EstimateServiceTests()
        {
            _estimateRepositoryMock = new Mock<IEstimateRepository>();
            _estimateServiceMock = new EstimateService(_estimateRepositoryMock.Object);
            _fixture = new Fixture();
        }

        [Fact(DisplayName = "GetEstimateById should return estimate by id successfully")]
        public async Task GetEstimateById_ShouldReturnCollegeSupplementalEssaySuccessfully()
        {
            //Arrange
            var estimateId = _fixture.Create<int>();
            var estimate = new EstimateResponseDto()
            {
                Id = estimateId
            };

            _estimateRepositoryMock.Setup(_ => _.GetEstimateById(estimateId)).ReturnsAsync(estimate);

            //Act
            var actual = await _estimateServiceMock.GetEstimateById(estimateId);

            //Assert
            actual.Id.Should().Equals(estimateId);
        }
    }
}
