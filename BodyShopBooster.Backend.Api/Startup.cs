using BodyShopBooster.Backend.Core.Data;
using BodyShopBooster.Backend.Data;
using BodyShopBooster.Backend.Data.Implementations;
using BodyShopBooster.Backend.Data.Interfaces;
using BodyShopBooster.Backend.Services.Implementations;
using BodyShopBooster.Backend.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace BodyShopBooster.Backend.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment envService)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(envService.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{envService.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            CurrentEnvironment = envService;
        }

        private IWebHostEnvironment CurrentEnvironment { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appSettings = new AppSettings();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            Configuration.GetSection("AppSettings").Bind(appSettings);

            AddDBContext(services);
           
            AddInjection(services);

            services.AddControllers();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            AddSwagger(services, appSettings);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            if (!env.IsProduction())
            {
                app.UseSwagger();
                app.UseSwaggerUI(setupAction =>
                {
                    setupAction.SwaggerEndpoint("/swagger/EstimateAPISpecification/swagger.json",
                        "Estimate Api");
                    setupAction.RoutePrefix = string.Empty;
                });
            }

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddDBContext(IServiceCollection services)
        {
            services.AddDbContext<EstimateDbContext>(opt =>
            {
                opt.UseSqlServer(Configuration.GetConnectionString("estimateDb"),
                    providerOptions => providerOptions.EnableRetryOnFailure());
                if (CurrentEnvironment.IsDevelopment()) opt.EnableSensitiveDataLogging();
            });
        }

        private void AddInjection(IServiceCollection services)
        {
            InjectServices(services);
            InjectRepositories(services);
        }

        private void InjectServices(IServiceCollection services)
        {
            services.AddScoped<IEstimateService, EstimateService>();
        }

        private void InjectRepositories(IServiceCollection services)
        {
            services.AddScoped<IEstimateRepository, EstimateRepository>();
        }

        private void AddSwagger(IServiceCollection services, AppSettings appSettings)
        {
            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc(appSettings.Swagger.Name,
                    new OpenApiInfo()
                    {
                        Title = appSettings.Swagger.Title,
                        Description = appSettings.Swagger.Description
                    });

                var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlCommentsFilePath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
                setupAction.IncludeXmlComments(xmlCommentsFilePath);
            });
        }
    }
}
