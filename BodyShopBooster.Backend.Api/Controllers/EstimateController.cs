﻿using BodyShopBooster.Backend.Data.Dtos;
using BodyShopBooster.Backend.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace BodyShopBooster.Backend.Api.Controllers
{
    /// <summary>
    /// Estimate Api controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class EstimateController : ControllerBase
    {
        private readonly IEstimateService _estimateService;
        private readonly ILogger<EstimateController> _logger;

        /// <summary>
        /// Initialize new instance of Estimate Api controller
        /// </summary>
        /// <param name="estimateService"></param>
        /// <param name="logger"></param>
        public EstimateController(IEstimateService estimateService, ILogger<EstimateController> logger)
        {
            _estimateService = estimateService;
            _logger = logger;
        }

        /// <summary>
        /// Add new estimate
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<IActionResult> PostEstimate([FromBody] EstimateRequestDto request)
        {
            try
            {
                await Task.Run(() => _estimateService.CreateEstimate(request));
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get estimate by Id
        /// </summary>
        /// <param name="estimateId"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(List<EstimateResponseDto>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<IActionResult> GetEstimateById([FromQuery(Name = "estimate-id")][Required] int estimateId)
        {
            try
            {
                var result = await Task.Run(() => _estimateService.GetEstimateById(estimateId));
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update existing estimate
        /// </summary>
        /// <param name="estimateId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [HttpPut("{estimate-id}")]
        public async Task<IActionResult> PutEstimate([FromRoute(Name = "estimate-id")][Required] int estimateId, [FromBody] EstimateRequestDto request)
        {
            try
            {
                await Task.Run(() => _estimateService.UpdateEstimate(estimateId, request));
                return NoContent();
            }
            catch (InvalidOperationException ioex)
            {
                _logger.LogError(ioex, ioex.Message);
                return BadRequest(ioex.Message);
            }
        }
    }
}
